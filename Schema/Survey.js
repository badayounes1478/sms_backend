const mongoose = require('mongoose')
const schema = mongoose.Schema

const Survey = new schema({
    hospitalId: String,
    name: {
        type: String,
        trim: true,
    },
    questions: [{
        question: String,
        stop: Number
    }],
    totalQuestions: Number,
    startSurvey: {
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = mongoose.model('Survey', Survey)