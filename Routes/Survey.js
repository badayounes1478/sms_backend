const express = require('express')
const router = express.Router()
const survey = require('../Schema/Survey')

router.get('/:id', async (req, res) => {
    try {
        const surveyData = await survey.find({ hospitalId: req.params.id })
        return res.status(200).json({
            message: "success",
            data: surveyData
        })
    } catch (error) {
        return res.status(500).json({
            error: error
        })
    }
})

router.post('/create', async (req, res) => {
    try {
        const surveyData = await survey.create(req.body)
        return res.status(201).json({
            message: "success",
            data: surveyData
        })
    } catch (error) {
        return res.status(500).json({
            error: error
        })
    }
})

module.exports = router